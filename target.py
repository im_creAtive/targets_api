from flask import Flask
from flask import request
from flask import send_file, make_response
from flask_jsonpify import jsonify
from flask import abort
import sqlite3
import time
import os

import config

app = Flask(__name__)

def dict_factory(cursor, row):
    d = {}
    for idx, col in enumerate(cursor.description):
        d[col[0]] = row[idx]
    return d

connection = sqlite3.connect('data.db', check_same_thread=False)
connection.row_factory = dict_factory
cur = connection.cursor()
cur.execute('CREATE TABLE IF NOT EXISTS `projects` (id INTEGER PRIMARY KEY AUTOINCREMENT, name VARCHAR(128), active BOOLEAN(1))')
cur.execute('CREATE TABLE IF NOT EXISTS `targets` (id INTEGER PRIMARY KEY AUTOINCREMENT, name VARCHAR(255), active BOOLEAN(1), project_id INT(11), url TEXT)')
cur.execute('CREATE TABLE IF NOT EXISTS `files` (id INTEGER PRIMARY KEY AUTOINCREMENT, target_id INT(11), path VARCHAR(255), type VARCHAR(16), os VARCHAR(32), name VARCHAR(255))')
connection.commit()


@app.route('/projects', methods=['GET'])
def projects():
    cur.execute('SELECT * FROM `projects`')
    projects = cur.fetchall()
    return jsonify(projects)


@app.route('/addProject', methods=['POST'])
def add_project():
    if not request.json or not 'name' in request.json or len(str(request.json['name'])) == 0:
        return jsonify({
            'error': 'Missing required `name` parameter',
            'code': 'ADD_PROJECT_NAME_REQ'
        })
    if not 'active' in request.json:
        active = 1
    else:
        active = request.json['active']
    try:
        cur.execute('INSERT INTO `projects` (name, active) VALUES (?,?)', (request.json['name'], active))
        connection.commit()
        return jsonify({
            'id': cur.lastrowid,
            'name': request.json['name'],
            'active': active
        })
    except:
        return jsonify({
            'error': 'Database error',
            'code': 'DB_ERROR'
        })


@app.route('/editProject/<int:project_id>', methods=['PUT'])
def edit_project(project_id):
    if not request.json or not 'name' in request.json or len(str(request.json['name'])) == 0:
        return jsonify({
            'error': 'Missing required `name` parameter',
            'code': 'EDIT_PROJECT_NAME_REQ'
        })
    else:
        name = str(request.json['name'])


    cur.execute("SELECT * FROM `projects` WHERE `id`=:id", {"id": project_id})
    connection.commit()
    data = cur.fetchone()

    if 'active' in request.json:
        data['active'] = request.json['active']

    data['name'] = name

    try:
        cur.execute('UPDATE `projects` SET `name`=:name,`active`=:active WHERE `id`=:id', {'name': data['name'], 'active': data['active'], 'id': data['id']})
        connection.commit()
        return jsonify({
            'id': data['id'],
            'name': data['name'],
            'active': data['active']
        })
    except:
        return jsonify({
            'error': 'Database error',
            'code': 'DB_ERROR'
        })


@app.route('/deleteProject/<int:project_id>', methods=['DELETE'])
def delete_project(project_id):
    try:
        cur.execute('DELETE FROM `projects` WHERE `id`=:id', {'id': project_id})
        connection.commit()
        return jsonify(None)
    except:
        return jsonify({
            'error': 'Database error',
            'code': 'DB_ERROR'
        })


@app.route('/project/<int:project_id>', methods=['GET'])
def get_project(project_id):
    try:
        cur.execute('SELECT * FROM `projects` WHERE `id`=:project_id', {'project_id': int(project_id)})
        connection.commit()
        data = cur.fetchone()
        if data and len(data) > 0:
            return jsonify(data)
        else:
            return jsonify({
                'error': 'Project not found',
                'code': 'PROJECT_NOT_FOUND'
            })
    except:
        return jsonify({
            'error': 'Database error',
            'code': 'DB_ERROR'
        })


@app.route('/targets/<int:project_id>', methods=['GET'])
def targets(project_id):
    cur.execute('SELECT * FROM `targets` WHERE `project_id`=:project_id', {'project_id': project_id})
    projects = cur.fetchall()
    return jsonify(projects)


@app.route('/addTarget', methods=['POST'])
def add_target():
    if not request.json or not 'project_id' in request.json:
        return jsonify({
            'error': 'Missing required `project_id` parameter',
            'code': 'ADD_TARGET_PROJECTID_REQ'
        })
    if not request.json or not 'name' in request.json:
        return jsonify({
            'error': 'Missing required `name` parameter',
            'code': 'ADD_TARGET_NAME_REQ'
        })
    if not request.json or not 'url' in request.json:
        return jsonify({
            'error': 'Missing required `url` parameter',
            'code': 'ADD_TARGET_URL_REQ'
        })
    if not request.json or not 'active' in request.json:
        request.json['active'] = 1

    cur.execute('SELECT `active` FROM `projects` WHERE `id`=:project_id AND `active`=1', {'project_id': request.json['project_id']})
    connection.commit()
    projects = cur.fetchall()
    if len(projects) > 0:
        try:
            cur.execute('INSERT INTO `targets` (`project_id`,`name`,`url`,`active`) VALUES (:project_id,:name,:url,:active)', {'project_id': request.json['project_id'], 'name': request.json['name'], 'url': request.json['url'], 'active': request.json['active']})
            connection.commit()
            return jsonify({
                'id': cur.lastrowid,
                'project_id': request.json['project_id'],
                'name': request.json['name'],
                'active': request.json['active']
            })
        except:
            return jsonify({
                'error': 'Database error',
                'code': 'DB_ERROR'
            })
    else:
        return jsonify({
            'error': 'project not found or inactive',
            'code': 'ADD_TARGET_PROJECT_NOT_FOUND'
        })


@app.route('/editTarget/<int:target_id>', methods=['PUT'])
def edit_target(target_id):
    cur.execute('SELECT * FROM `targets` WHERE `id`=:target_id', {'target_id': int(target_id)})
    connection.commit()
    data = cur.fetchone()

    if data and len(data) > 0:
        if 'name' in request.json and len(str(request.json['name'])) > 0:
            data['name'] = request.json['name']
        if 'url' in request.json:
            data['url'] = request.json['url']
        if 'active' in request.json:
            data['active'] = request.json['active']
        try:
            cur.execute('UPDATE `targets` SET `name`=:name,`name`=:name,`url`=:url,`active`=:active WHERE `id`=:target_id', {
                'name': data['name'],
                'url': data['url'],
                'active': data['active'],
                'target_id': data['id']
            })
            connection.commit()
            return jsonify({
                'id': data['id'],
                'name': data['name'],
                'project_id': data['project_id'],
                'url': data['url'],
                'active': data['active']
            })
        except:
            return jsonify({
                'error': 'Database error',
                'code': 'DB_ERROR'
            })
    else:
        return jsonify({
            'error': 'Target not found',
            'code': 'EDIT_TARGET_NOT_FOUND'
        })


@app.route('/deleteTarget/<int:target_id>', methods=['DELETE'])
def delete_target(target_id):
    try:
        cur.execute('DELETE FROM `targets` WHERE `id`=:id', {'id': target_id})
        connection.commit()
        return jsonify(None)
    except:
        return jsonify({
            'error': 'Database error',
            'code': 'DB_ERROR'
        })


@app.route('/target/<int:target_id>', methods=['GET'])
def get_target(target_id):
    try:
        cur.execute('SELECT * FROM `targets` WHERE `id`=:target_id AND `active`=1', {'target_id': int(target_id)})
        connection.commit()
        data = cur.fetchone()
        data['files'] = []

        if data and len(data) > 0:
            data['files'] = {'videos': [], 'androidBundle': '', 'iOSBundle': '', 'standAloneBundle': ''}
            cur.execute('SELECT * FROM `files` WHERE `target_id`=:target_id', {'target_id': target_id})
            connection.commit()
            files = cur.fetchall()

            for file in files:
                filepath = config.appHostname + ":" + str(config.appPort) + '/target/downloadFile/' + str(file['id'])

                if (file['type'] == 'video'):
                    data['files']['videos'].append({'name': file['name'], 'link': filepath})
                elif file['type'] == 'bundle' and file['os'] == 'android':
                    data['files']['androidBundle'] = filepath
                elif file['type'] == 'bundle' and file['os'] == 'ios':
                    data['files']['iOSBundle'] = filepath
                elif file['type'] == 'bundle' and file['os'] == 'standalone':
                    data['files']['standAloneBundle'] = filepath

            return jsonify(data)
        else:
            return jsonify({
                'error': 'Target not found or inactive',
                'code': 'TARGET_NOT_FOUND'
            })
    except:
        return jsonify({
            'error': 'Database error',
            'code': 'DB_ERROR'
        })

@app.route('/targets/search/<string:search>', methods=['GET'])
def search_targets(search):
    try:
        cur.execute("SELECT * FROM `targets` WHERE `name` LIKE :search AND `active`=1", {'search': '%'+search+'%'})
        connection.commit()
        data = cur.fetchall()

        for target in data:
            target['files'] = {'videos': [], 'androidBundle': '', 'iOSBundle': '', 'standAloneBundle': ''}
            cur.execute('SELECT * FROM `files` WHERE `target_id`=:target_id', {'target_id': target['id']})
            connection.commit()
            files = cur.fetchall()

            for file in files:
                filepath = config.appHostname + ":" + str(config.appPort) + '/target/downloadFile/' + str(file['id'])

                if(file['type']=='video'):
                    target['files']['videos'].append({'name': file['name'], 'link': filepath})
                elif file['type'] == 'bundle' and file['os'] == 'android':
                    target['files']['androidBundle'] = filepath
                elif file['type'] == 'bundle' and file['os'] == 'ios':
                    target['files']['iOSBundle'] = filepath
                elif file['type'] == 'bundle' and file['os'] == 'standalone':
                    target['files']['standAloneBundle'] = filepath

        return jsonify(data)
    except:
        return jsonify({
            'error': 'Database error',
            'code': 'DB_ERROR'
        })


@app.route('/target/<int:target_id>/uploadBundle/<string:osType>', methods=['POST'])
def upload_bundle_file_to_target(target_id,osType):
    if request.files and 'file' in request.files:
        file = request.files["file"]
        file_dir = 'storage/'+str(target_id)+'/'
        file_name = osType+'_'+file.filename

        if osType != 'android' and osType != 'ios' and osType != 'standalone':
            return jsonify({
                'error': 'OS type must be one of (android|ios|standalone)',
                'code': 'UPLOAD_FILE_OS_INVALID'
            })

        if not os.path.exists(file_dir):
            os.makedirs(file_dir)
        try:
            file.save(file_dir+file_name)

            cur.execute('SELECT `id` FROM `files` WHERE `target_id`=:target_id AND `path`=:path', {'target_id': target_id, 'path': file_name})
            connection.commit()

            data = cur.fetchone()

            if data and len(data) > 0:
                file_id = data['id']
            else:
                cur.execute('INSERT INTO `files` (`target_id`,`path`,`type`,`os`,`name`) VALUES (:target_id,:path,"bundle",:os,:name)', {'target_id': target_id, 'path': file_name, 'os': osType, 'name': file_name})
                connection.commit()
                file_id = cur.lastrowid
        except:
            return jsonify({
                'error': 'upload error',
                'code': 'UPLOAD_FILE_ERROR'
            })
        return jsonify({
            'file': config.appHostname+":"+str(config.appPort)+'/target/downloadFile/'+str(file_id)
        })
    else:
        return jsonify({
            'error': 'File is empty',
            'code': 'UPLOAD_FILE_EMPTY'
        })


@app.route('/target/<int:target_id>/uploadVideo/<string:name>', methods=['POST'])
def upload_video_file_to_target(target_id,name):
    if request.files and 'file' in request.files:
        file = request.files["file"]
        file_dir = 'storage/'+str(target_id)+'/'
        file_name = file.filename

        if not os.path.exists(file_dir):
            os.makedirs(file_dir)
        try:
            file.save(file_dir+file_name)

            cur.execute('SELECT `id` FROM `files` WHERE `target_id`=:target_id AND `path`=:path', {'target_id': target_id, 'path': file_name})
            connection.commit()

            data = cur.fetchone()

            if data and len(data) > 0:
                file_id = data['id']
            else:
                cur.execute('INSERT INTO `files` (`target_id`,`path`,`type`,`os`,`name`) VALUES (:target_id,:path,"video","all",:name)', {'target_id': target_id, 'path': file_name, 'name': name})
                connection.commit()
                file_id = cur.lastrowid
        except:
            return jsonify({
                'error': 'upload error',
                'code': 'UPLOAD_FILE_ERROR'
            })
        return jsonify({
            'file': config.appHostname+":"+str(config.appPort)+'/target/downloadFile/'+str(file_id)
        })
    else:
        return jsonify({
            'error': 'File is empty',
            'code': 'UPLOAD_FILE_EMPTY'
        })


@app.route('/target/downloadFile/<int:file_id>', methods=['GET'])
def download_file_from_target(file_id):
    cur.execute('SELECT * FROM `files` WHERE `id`=:file_id', {'file_id': int(file_id)})
    connection.commit()
    data = cur.fetchone()

    if data and len(data) > 0:
        try:
            response = make_response(send_file('storage/'+str(data['target_id'])+'/'+str(data['path'])))
            response.headers["Content-Disposition"] = 'attachment; filename="%s"' % (str(data['path']))
            return response
        except:
            return jsonify({
                'error': 'download error',
                'code': 'DOWNLOAD_ERROR'
            })
    else:
        return jsonify({
            'error': 'file not found',
            'code': 'DOWNLOAD_FILE_NOT_FOUND'
        })

@app.route('/target/deleteFile/<int:file_id>', methods=['DELETE'])
def delete_file_from_target(file_id):
    cur.execute('SELECT * FROM `files` WHERE `id`=:file_id', {'file_id': int(file_id)})
    connection.commit()
    data = cur.fetchone()

    if data and len(data) > 0:
        try:
            os.remove('storage/'+str(data['target_id'])+'/'+str(data['path']))
        except:
            return jsonify({
                'error': 'Remove error',
                'code': 'DELETE_FILE_ERROR'
            })

        cur.execute('DELETE FROM `files` WHERE `id`=:file_id', {'file_id': int(file_id)})
        connection.commit()
        return jsonify(None)
    else:
        return jsonify({
            'error': 'file not found',
            'code': 'DELETE_FILE_NOT_FOUND'
        })

if __name__ == '__main__':
    app.run(host=config.appHost,debug=config.appDebug, port=config.appPort, threaded=True)


